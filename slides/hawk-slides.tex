\input{preamble}

\begin{document}

\maketitle

\begin{frame}{Who are we? --- Hawk team}
\centering

\begin{columns}
\column{.3\textwidth}
\centering
\includegraphics[height=.25\textheight]{biopic-01-antonio}
\column{.7\textwidth}
\begin{block}{Antonio (Lecturer, Aston University)}
\begin{itemize}
\item Hawk project lead
\item Eclipse Epsilon committer
\end{itemize}
\end{block}
\end{columns}

\begin{columns}
\column{.3\textwidth}
\centering
\includegraphics[height=.25\textheight,clip,trim={0 3cm 0 0}]{biopic-02-konstantinos}
\column{.7\textwidth}
\begin{block}{Konstantinos (Research Associate, U.\ of York)}
\begin{itemize}
\item Hawk initiator (core developer)
\item Created most core components
\end{itemize}
\end{block}
\end{columns}

\begin{columns}
\column{.3\textwidth}
\centering
\includegraphics[height=.25\textheight,clip,trim={0 4cm 0 0}]{biopic-03-dimitris}
\column{.7\textwidth}
\centering
\begin{block}{Dimitris (Professor, University of York)}
\begin{itemize}
\item Hawk initiator (MONDO WP lead)
\item Eclipse Epsilon project lead
\end{itemize}
\end{block}
\end{columns}

\end{frame}

\begin{frame}{Motivation: monolithic XMI models do not scale}
\centering

\begin{table}
\begin{tabular}{rrrr}
\toprule
Model & XMI (MB) & Avg.\ memory (MB) & Max memory (MB) \\
\midrule
%set0 & 8.75 & 22 & 49 \\
set1 & 26.59 & 53 & 135 \\
set2 & 270.12 & 437 & 840 \\
set3 & 597.67 & 849 & 1798 \\
set4 & 645.53 & 949 & 1910 \\
\bottomrule
\end{tabular}

\caption{Querying GraBaTs'09 Java models to find singletons}
\end{table}

\end{frame}

\begin{frame}{What to do?}

\begin{block}{Option 1: break up into fragments}
  \begin{itemize}
  \item Code is broken up into modules --- do the same with models
  \item Fragmented file-based models play well with traditional VCS
  \item Global queries may still require loading all fragments!
  \item \textbf{\alert{Hawk}} is our solution: indexes the fragments into a NoSQL database, queries the database instead of the fragments
  \end{itemize}
\end{block}

\begin{block}{Option 2: stop using files}
  \begin{itemize}
  \item Obviously not new: CDO has been doing it for years
  \item  However, relational DBs are not the only option
  \item NeoEMF can replace traditional XMI persistence with NoSQL approaches:
    graph databases, key-value stores...
  \item This potentially requires significant rework, though
  \end{itemize}
\end{block}

\end{frame}

\begin{frame}{Hawk: indexing for fast querying over fragment collections}
  \centering

  \begin{columns}[t]
    \column{.5\textwidth}
    \centering
    \begin{tikzpicture}[remember picture]
      \node (oldapproach) {\includegraphics[width=\columnwidth,height=4.15cm,keepaspectratio]{old-approach}};
    \end{tikzpicture}
    \begin{block}{Usual approach}
      \begin{enumerate}
      \item Check out \textbf{all} files from VCS
      \item Load fragments into memory
      \item Run query (might go over all fragments)
      \end{enumerate}
    \end{block}

    \column{.5\textwidth}
    \centering
    \begin{tikzpicture}[remember picture]
      \node (newapproach) {\includegraphics[width=\columnwidth,height=4.15cm,keepaspectratio]{hawk-approach}};
    \end{tikzpicture}
    \begin{block}{With Hawk}
      \begin{enumerate}
      \item Hawk watches VCS, indexes
      \item User queries Hawk over WS
      \item Hawk runs query through NoSQL database efficiently
      \item Hawk replies with result
      \end{enumerate}
    \end{block}

  \end{columns}

  \begin{tikzpicture}[overlay,remember picture]
    \draw[ultra thick,red,->] (oldapproach) -> (newapproach);
  \end{tikzpicture}
\end{frame}

\begin{frame}{Deployment}

  \begin{center}
    \includegraphics[width=\textwidth]{hawk-deployment}
  \end{center}

  \begin{itemize}
  \item Hawk can run as Eclipse plug-in, Java library, or network service
  \item We can have it watch over various types of locations:
    \begin{itemize}
    \item Version control systems (SVN/Git repositories)
    \item File stores (local folders, Eclipse workspaces, HTTP locations)
    \end{itemize}
  \end{itemize}

\end{frame}

\begin{frame}{Component-based architecture}
  \begin{center}
    \includegraphics[width=.6\textwidth,height=.6\textheight,keepaspectratio]{hawk-architecture}
  \end{center}

  \begin{itemize}
  \item Core: incremental graph updating + component interfaces
  \item Backends: Neo4j (fastest), OrientDB (multi-master), Greycat
  \item Clients: Eclipse GUI, cross-language Apache Thrift web services
  \item Query engines: Epsilon Object/Pattern Languages, OrientDB SQL
  \item Model parsers: EMF/Modelio models, Eclipse plug-in manifests...
  \end{itemize}
\end{frame}

\begin{frame}{Example for a library model}
  \centering
  \includegraphics[width=\textwidth]{hawk-library-cropped}
  \begin{overprint}
    \onslide<1>
    \begin{tikzpicture}[remember picture,overlay]
      \draw[fill=white,fill opacity=0.8,draw=none]
      (page cs:-0.345,-0.4) rectangle (page cs:0.9,0.78);
    \end{tikzpicture}
    \begin{center}
      We go from these model files...
    \end{center}

    \onslide<2>
    \begin{tikzpicture}[remember picture,overlay]
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:-1.0,0.78) rectangle (page cs:-0.345,-0.4);
    \end{tikzpicture}
    \begin{center}
      ... to these NoSQL graphs.
    \end{center}

    \onslide<3>
    \begin{tikzpicture}[remember picture,overlay,every node/.style={draw=red,very thick},every edge/.style={draw=red,very thick,->}]
      %\printtikzpagegrid
      % \node[minimum width=2em,minimum height=.8em] (nbook) at (page cs:-.675,.405) {};
      % \node[minimum width=6.5em,minimum height=.9em] (nlibrary) at (page cs:-.65,.53) {};
      % \node[minimum width=2em,minimum height=.8em] (nauthor) at (page cs:-.515,.205) {};
      % \node[minimum width=2em,minimum height=.8em] (nbooki) at (page cs:-.71,-.19) {};
      % \node[minimum width=2em,minimum height=.8em] (nauthori) at (page cs:-.47,-.29) {};
      % \node[minimum width=3em,minimum height=.8em] (nmetamodel) at (page cs:.57,.57) {};
      % \node[minimum width=2em,minimum height=.8em] (nbookt) at (page cs:.44,.27) {};
      % \node[minimum width=2em,minimum height=.8em] (nauthort) at (page cs:.695,.27) {};
      % \node[minimum width=2em,minimum height=.8em] (nbooke) at (page cs:.435,.02) {};
      % \node[minimum width=2em,minimum height=.8em] (nauthore) at (page cs:.695,.02) {};
      % \node[minimum width=2em,minimum height=.8em] (nfile) at (page cs:.57,-.2) {};
      % \node[minimum width=6.5em,minimum height=.8em] (nmetamodelkey) at (page cs:-.062,.485) {};
      % \node[minimum width=6.5em,minimum height=.8em] (nfilekey) at (page cs:-.062,-.015) {};
      % \path
      %   (nlibrary) edge[out=0,in=180] (nmetamodel)
      %   (nbook) edge[out=0,in=180] (nbookt)
      %   (nauthor) edge[out=0,in=200] (nauthort);

      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:-0.9,-.4) rectangle (page cs:-.35,.02);
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:-0.345,-.4) rectangle (page cs:0.34,0.78);
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:0.34,-.4) rectangle (page cs:0.85,0.15);
    \end{tikzpicture}
    \begin{itemize}
    \item Ecore packages $\rightarrow$ metamodel nodes
    \item Ecore classes $\rightarrow$ type nodes
    \end{itemize}

    \onslide<4>
    \begin{tikzpicture}[remember picture,overlay]
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:-.35,.02) rectangle (page cs:-0.9,.78);
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:-0.345,-0.4) rectangle (page cs:0.34,0.78);
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:0.85,0.15) rectangle (page cs:0.34,0.78);
    \end{tikzpicture}
    \begin{itemize}
    \item Physical files $\rightarrow$ file nodes
    \item Model elements $\rightarrow$ element nodes
    \end{itemize}

    \onslide<5>
    \begin{tikzpicture}[remember picture,overlay]
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:-.35,-.4) rectangle (page cs:-0.9,.78);
      \draw[fill=white,fill opacity=0.8,draw=none]
        (page cs:0.85,-.4) rectangle (page cs:0.34,0.78);
    \end{tikzpicture}
    \begin{itemize}
    \item MM index: package URI $\rightarrow$ metamodel node
    \item File index: file path $\rightarrow$ file node
    \item Users can define custom indices by attribute/expression
    \end{itemize}
  \end{overprint}
\end{frame}

\begin{frame}[standout]
  Demo time!

  Let's index a Java model and find singletons.
\end{frame}

\begin{comment}

\newcommand{\screenshot}[3]{
  \begin{frame}{#1}
    \begin{center}
      \includegraphics[width=\textwidth, height=.7\textheight, keepaspectratio]{#2}

      #3
    \end{center}
  \end{frame}
}

\screenshot{Preparing the workspace}{00-workspace}{
  Open Eclipse and import \file{tutorial.common} and \file{tutorial.hawk}.
}

\screenshot{Opening the main Hawk view}{01-open-view}{}

\screenshot{Adding a new Hawk index}{02-add}{
  Click on the ``Add'' button. We can keep as many different indices as we want
  within the same Eclipse installation.
}

\screenshot{Options, options!}{03-add-dialog}{
  There are many options here: we can use a local or a remote Hawk, we can
  choose the backend, and we can enable/disable plugins at will.
}

\screenshot{Options for the JDTAST models}{04-add-dialog-filled}{
  We will go with a local, Neo4j-based, EMF-focused index.
}

\screenshot{Index created}{05-created}{
  We have created an empty index. Time to configure it.
}

\screenshot{Configuring the index}{06-configure-button}{
  Let's click on the ``Configure'' button. This will open a new dialog.
}

\screenshot{Configure dialog}{06-configure-dialog}{
  The dialog is divided into tabs for managing metamodels, locations to index,
  and indexed/derived properties.
}

\screenshot{Adding metamodels}{07-add-mm-workspace}{
  Click on ``Add from workspace'' and select the Ecore, XMLTypes and
  JDTAST metamodels. These will be turned into Neo4j nodes and edges.
}

\screenshot{Added metamodels}{08-added-mm}{
  The metamodels have been registered: we can index models now.
}

\screenshot{Add locations}{09-add-locations}{
  Go to the ``Indexed Locations'' tab and click on ``Add''.
}

\screenshot{Adding a local folder}{10-localfolder}{
  We want to index the ``LocalFolder'' in the \file{tutorial.common/models/set1} folder.
}

\screenshot{Added local folder}{11-added-folder}{
  Wait a bit, watching the ``Status'' column, and the models will be indexed.
  Once it's done, the index is ready to be queried.
}

\screenshot{Query button}{12-query-button}{
  Click on the ``Query'' button.
}

\screenshot{Query dialog: main tab}{13-query-dialog}{
  The ``Query'' dialog is quite involved: let's go bit by bit.

  The main page allows us to enter the query by hand, from a file or from the
  current editor, run/stop/reset it, and request manual syncs.
}

\screenshot{Query dialog: path-based scope}{14-path-scope}{
  We could optionally limit the scope of the query to specific repositories and/or files.
  This can be useful when querying large collections of models.

  Patterns are essentially glob-style: e.g. \file{folder/f*.xmi}.
}

\screenshot{Query dialog: subtree-based scope}{15-subtree-scope}{
  Another option is to limit scope to the objects contained within a certain top-level
  file. This can be useful when querying hierarchically fragmented models (e.g. those
  created with EMF-Splitter by Antonio Garmendia).
}

\screenshot{Query dialog: default namespaces}{16-default-ns}{
  In some cases, ``Type.all'' might be ambiguous as we may have multiple types
  with the same unqualified name. Here we can specify which metamodel URIs should
  take precedence.
}

\screenshot{Query dialog: traversal scoping}{17-traversal-scoping}{
  When following references from an object, we may want to ignore the ones that leave
  the path-/subtree-based scope that we defined before.
}

\screenshot{Query dialog: entering a query}{18-query-entered}{
  Let's go back to the main tab and enter a simple query about the number of instances
  of a class. Click on ``Run Query'', which will temporarily change to ``Stop Query''.
}

\screenshot{Query dialog: query executed}{19-query-executed}{
  The query finished running, and here we have the results.

  Now that we know the basic use of Hawk, let's move to the more advanced features.
}

\end{comment}

\begin{frame}[fragile]{Indexed attributes}
  \begin{block}{Finding a type by its name (\file{findByName.eol})}
    \lstinputlisting[language=EOL]{listings/set0-findByName.eol}
  \end{block}

  \begin{block}{This normally involves...}
    \begin{enumerate}
    \item Iterating over all types
    \item Following the ``name'' reference
    \item Comparing the name
    \end{enumerate}
  \end{block}

  \begin{alertblock}{Replace with a lookup (\file{findByName-indexed.eol})}
    \vspace{.3em}
    We only need to tell it to index ``SimpleName.identifier'':
    \lstinputlisting[language=EOL]{listings/set0-findByName-indexed.eol}
  \end{alertblock}
\end{frame}

\begin{comment}

\screenshot{Indexed attributes tab}{22-iattr-tab}{
  Go back to the ``Configure'' tab and select the ``Indexed Attributes''
  tab. Click on ``Add'', and a new dialog should pop up.
}

\screenshot{Options for the indexed attribute}{23-iattr-dialog}{
  Use the combo boxes to pick the metamodel, type and attribute
  shown above. Click on ``Add''.
}

\end{comment}

\begin{frame}[fragile]{Derived attributes}
  \begin{block}{Original query for finding singletons (\file{singletons.eol})}
    \lstinputlisting[language=EOL]{listings/singletons.eol}
  \end{block}

  \begin{block}{Can we do it faster?}
    \begin{itemize}
    \item Checking if a method is public or static requires traversing
      references
    \item Same goes for checking if it returns an instance of itself
    \item In Hawk, \alert{we can precompute this}
    \item When files change, only the affected values are recomputed
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Use of derived attributes as precomputed values}

  \begin{block}{Original query (\file{singletons.eol})}
    \lstinputlisting[language=EOL]{listings/singletons.eol}
  \end{block}

  \begin{block}{Changed for MethodDeclaration derived attributes (\file{singletons-dmethods.eol})}
    \lstinputlisting[language=EOL,firstline=13]{listings/singletons-dmethods.eol}
  \end{block}
\end{frame}

\begin{comment}

\screenshot{Derived attributes tab}{25-dattr-tb}{
  Now we move to the ``Derived Attributes'' tab of the configure dialog.
}

\screenshot{Options for one of the derived attributes}{26-dattr-dialog}{
  For the first derived attribute, fill in the form as above. You can load the
  EOL code straight from the workspace --- no need to copy and paste!
}

\end{comment}

\begin{frame}{Derived attributes are also indexed}
  \begin{block}{Revised query (\file{singletons-dmethods.eol})}
    \lstinputlisting[language=EOL,firstline=13]{listings/singletons-dmethods.eol}
  \end{block}

  \begin{block}{Can we do it faster?}
    \begin{itemize}
    \item Right now, we need to go through \emph{all} type declarations and then
      filter by methods
    \item What if we go from the methods to the types instead?
    \item In Hawk, \alert{top-level selects can replace iteration with lookups
      when using derived attributes}
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}{Use of derived attributes as index keys}

  \begin{block}{Previous query (\file{singletons-dmethods.eol})}
    \lstinputlisting[language=EOL,firstline=13]{listings/singletons-dmethods.eol}
  \end{block}

  \begin{block}{Revised to use index, with derived attributes at top level (\file{singletons-dmethodsindexed.eol})}
    \lstinputlisting[language=EOL]{listings/singletons-dmethodsindexed.eol}
  \end{block}

\end{frame}

\begin{frame}[fragile]{Flagging singletons directly}
  \begin{block}{Previous query (\file{singletons-dmethodsindexed.eol})}
    \lstinputlisting[language=EOL]{listings/singletons-dmethodsindexed.eol}
  \end{block}

  \begin{block}{Can we do it faster?}
    \begin{itemize}
    \item We could just flag types that are singletons
    \item This derived attribute might be less reusable, however
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[fragile]{Final query for finding singletons}
  \begin{block}{Previous query (\file{singletons-dmethodsindexed.eol})}
    \lstinputlisting[language=EOL]{listings/singletons-dmethodsindexed.eol}
  \end{block}

  \begin{block}{Final query (\file{singletons-dtypes.eol})}
    \lstinputlisting[language=EOL,firstline=12]{listings/singletons-dtypes.eol}
  \end{block}
\end{frame}

\begin{frame}[standout]
  Demo time!

  This time, we will show how to use indexed and derived attributes.
\end{frame}

\begin{frame}[fragile]{Derived edges}
  \begin{block}{Toy example: Person metamodel}
    \begin{itemize}
    \item Person metamodel, with ``parents'' references.
    \item We want to be able to quickly find siblings, grandparents,
      uncles/aunts, cousins, second-cousins, ancestors...
    \item We can precompute this in Hawk with \alert{derived edges}
    \end{itemize}
  \end{block}

  \begin{block}{Derivation logic for ``grandparents'' (\file{Person\_grandparents.eol})}
    \vspace{.3em}
    We need a flat list and not a list of lists, so we use ``flatten'':
    \begin{lstlisting}[language=EOL]
      return self.parents.parents.flatten;
    \end{lstlisting}
  \end{block}

  \begin{block}{Derivation logic for ``siblings'' (\file{Person\_siblings.eol})}
    \vspace{.3em}
    We can travel references in reverse with ``revRefNav\_name'':

    \begin{lstlisting}[language=EOL]
      return self.parents.revRefNav_parents.flatten.excluding(self);
    \end{lstlisting}
  \end{block}

\end{frame}

\begin{frame}[standout]
  Last demo: let's use derived edges.
\end{frame}

\begin{frame}{Hawk: integration into SOFTEAM Constellation (2016)}

  \centering
  \begin{columns}
    \column{.5\textwidth}
    \input{figures/hawkmodelio-indexingtime}

    \column{.4\textwidth}
    \input{figures/hawkmodelio-docgentime}
  \end{columns}

  \begin{itemize}
  \item Constellation: collaboration platform over Modelio models
  \item SOFTEAM needed search, couldn't change persistence
  \item Integrated Hawk as a library: initial indexing cost quickly paid off
  \end{itemize}
\end{frame}

\begin{comment}

\begin{frame}{Stress-testing remote query APIs (2017)}

  \begin{center}
    \includegraphics[width=\textwidth]{rq3-comparison-tb}
  \end{center}

  \begin{itemize}
  \item Included CDO, Hawk, Mogwaï and ranged 1--64 clients
  \item Reverse reference navigation was crucial in having the
    SN Train Benchmark query run quickly
  \end{itemize}

\end{frame}

\end{comment}

\begin{frame}[fragile]{Time-aware queries in Hawk over versioned models (2018)}

  \begin{lstlisting}[frame=tb,language=EOL,escapechar=@,basicstyle=\footnotesize]
var rs = RewardTableRow@\textcolor{red}{.latest}@.all.collect(row | row.getRewardShifts()).flatten();
return Sequence { rs.min(), rs.max(), rs.average() };

operation RewardTableRow getRewardShifts(): Sequence {
  var v = self@\textcolor{red}{.versions}@;
  if (v.size <= 1) { return Sequence {}; }
  else { return v.subList(0, v.size - 1).collect(v | v.value - v@\textcolor{red}{.prev}@.value); }
}
operation Sequence average() { return self.sum() / self.size(); }
  \end{lstlisting}

  \begin{itemize}
  \item Extended Hawk with Greycat temporal graph support and time-aware query
    engine / updater components
  \item Can index entire SVN/Git-based history of a model, and ask things about
    its history through a new set of time-aware primitives
  \item Above query is finding descriptive stats for reward table shifts in a
    models@run.time system
  \end{itemize}

\end{frame}

\begin{frame}{Annotating histories with events of interest (2019)}

  % Cost of adding indexing into temporal graph: 12% extra time in simulation

  \pgfplotstableread[col sep=comma]{data/LTL_LongTermEffectMR.csv}\ltltimes
  \pgfplotstableread[col sep=comma]{data/LTL_LongTermEffectMR-Annotated.csv}\annotimes
  \pgfplotsset{colormap/Pastel1-5,colormap/Set1-5}

  \begin{figure}
    \begin{tikzpicture}
      \begin{semilogyaxis}[
          xlabel=Timeslice, ylabel={Time (ms)},
          no markers,ymax=2500,ymin=1,
          each nth point={10},
          height=4.5cm,
          legend columns=1,
          legend style={at={(1.8, 0.9)}, anchor=north west},
          width=.45\textwidth,
          title={Without timeline annotation},
        ]
        %\addplot[draw=Set1-A] table [x=NTimeSlice,y=TotalTimeTs] {\ltltimes};
        %\addlegendentry{Total}
        \addplot[densely dashed,draw=Set1-B] table [x=NTimeSlice,y=LiveUpdateLauncherSystemTime] {\ltltimes};
        \addlegendentry{Update}
        \addplot[densely dotted,draw=Set1-A] table [x=NTimeSlice,y=QueryTotalTimeSystem] {\ltltimes};
        \addlegendentry{Query}
      \end{semilogyaxis}
    %% \end{tikzpicture}
    %% \caption{Without timeline annotation}
    %% \label{fig:ltl-exec-noann}
    %% \begin{tikzpicture}
      \begin{semilogyaxis}[
          xlabel=Timeslice,
          no markers,ymax=2500,ymin=1,
          each nth point={10},
          height=4.5cm,
          width=.45\textwidth,
          at={(.46\textwidth,0)},
          title={With timeline annotation},
        ]
        %\addplot[draw=Set1-A] table [x=NTimeSlice,y=TotalTimeTs] {\annotimes};
        \addplot[densely dashed,draw=Set1-B] table [x=NTimeSlice,y=LiveUpdateLauncherSystemTime] {\annotimes};
        \addplot[densely dotted,draw=Set1-A] table [x=NTimeSlice,y=QueryTotalTimeSystem] {\annotimes};
      \end{semilogyaxis}
    \end{tikzpicture}
    %% \caption{With timeline annotation}
    %% \label{fig:ltl-exec-ann}
  \end{figure}

  \begin{itemize}
  \item Case study: do the decisions of a self-adaptive system with short-term
    losses have the expected long-term benefits?
  \item We queried the history after each timeslice of the simulation and found
    queries became much slower over time
  \item Hawk gained a new type of derived attribute: a \emph{timeline
    annotation}
  \item 99th percentile query time went from 148ms to 17ms
  \end{itemize}

  % Without timeline annotation, query time steadily increases. With timeline
  % annotation, query time does not increase nearly as much: 99-percentile query
  % time goes from 148ms to 17ms. (what happens with the update times?)

\end{frame}

\begin{frame}{Supporting Complex Event Processing as a filter (2021)}

  \begin{center}
    \includegraphics[width=\textwidth]{figures/ETeMoX}
  \end{center}

  \begin{itemize}
  \item Hawk was used to follow the training of several Reinforcement Learning
    approaches (Q-Learning, SARSA, Deep Q-Networks)
  \item Large throughputs and data volumes from the RL approaches (in GBs):
    indexing all history can be costly!
  \item Integrated the open-source Esper CEP engine with Hawk using MQTT: Esper
    samples/filters history to be indexed
  \end{itemize}

\end{frame}

\begin{frame}{Hawk: project website}
  \begin{center}
    \includegraphics[width=\textwidth]{hawk-eclipse-website}
  \end{center}

  \begin{itemize}
  \item \url{https://www.eclipse.org/hawk/}
  \item Eclipse Incubator project: latest stable release at 2.1.0
  \end{itemize}
\end{frame}

\begin{frame}{Hawk: summing up}

  \begin{block}{So far...}
    \begin{itemize}
    \item Hawk is good for indexing an existing collection of model files
    \item You can efficiently answer queries from the index
    \item Indexed/derived features can be used to speed up queries
    \item History can be indexed, annotated, filtered and sampled
    \end{itemize}
  \end{block}

  \begin{block}{Ideas in the roadmap}
    \begin{itemize}
    % Folding Space immediately asked about horizontal scaling, interestingly
    \item Horizontal scaling (a flock of Hawks?)
    \item Web UI based on Thrift API
    \item More backends! (Triple stores? Neo4j 4.x? MapDB?)
    \item Visualisations and datasets from time-aware queries (Picto, Pinset)
    \end{itemize}
  \end{block}

\end{frame}

\begin{frame}[standout]
  Thank you!

  %% \IfFileExists{\jobname-copy.pdf}{%
  %%   \begin{center}
  %%     \foreach \i in {4,6,9,12,14,17,22,29,30} {
  %%       \hyperlink{page.\i}{\includegraphics[width=0.3\linewidth, page=\i]{\jobname-copy.pdf}}
  %%     }
  %%   \end{center}
  %% }{\typeout{No Thumbnails included}}

  \vspace{-.5em}
  {\normalsize @antoniogado / a.garcia-dominguez@aston.ac.uk}

\end{frame}

\end{document}
